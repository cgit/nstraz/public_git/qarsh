
.PHONY: clean clobber uninstall

CFLAGS := -Wall -g -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE

COMMON := qarsh_packet.c sockutil.c

TARGETS := qarshd qarsh qacp btimed btimec

all: $(TARGETS)

qarshd: qarshd.c $(COMMON) 
qarsh: qarsh.c $(COMMON) btime.c hbeat.c
qacp: qacp.c $(COMMON)
btimed: btimed.c
btimec: btimec.c btime.c

install: all
	install -Dp qacp $(INSTROOT)/usr/bin/qacp
	install -Dp qarsh $(INSTROOT)/usr/bin/qarsh
	install -Dp qarshd $(INSTROOT)/usr/sbin/qarshd
	install -Dpm 0644 qarshd.socket $(INSTROOT)/usr/lib/systemd/system/qarshd.socket
	install -Dpm 0644 qarshd@.service $(INSTROOT)/usr/lib/systemd/system/qarshd@.service
	install -Dp btimed $(INSTROOT)/usr/sbin/btimed
	install -Dpm 0644 btimed.socket $(INSTROOT)/usr/lib/systemd/system/btimed.socket
	install -Dpm 0644 btimed.service $(INSTROOT)/usr/lib/systemd/system/btimed.service
	install -Dp btimec $(INSTROOT)/usr/bin/btimec
	install -Dpm 0644 qarsh.1 $(INSTROOT)/usr/share/man/man1/qarsh.1
	install -Dpm 0644 qacp.1 $(INSTROOT)/usr/share/man/man1/qacp.1
	install -Dpm 0644 btimec.1 $(INSTROOT)/usr/share/man/man1/btimec.1
	install -Dpm 0644 qarshd.8 $(INSTROOT)/usr/share/man/man8/qarshd.8
	install -Dpm 0644 btimed.8 $(INSTROOT)/usr/share/man/man8/btimed.8

clean:
	$(RM) $(TARGETS)

VERSION := $(shell awk '/^Version:/ { print $$2 }' qarsh.spec)

ifdef DIST
  RPMDEFS := -D "dist $(DIST)"
endif

QARSH_TARNAME=qarsh-$(VERSION)

$(QARSH_TARNAME).tar.bz2: qarsh.spec
	git archive --format=tar --prefix=$(QARSH_TARNAME)/ HEAD^{tree} > $(QARSH_TARNAME).tar
	bzip2 -f -9 $(QARSH_TARNAME).tar

tarball: $(QARSH_TARNAME).tar.bz2

rpm: $(QARSH_TARNAME).tar.bz2 qarsh.spec
	rpmbuild -ta $< $(RPMDEFS)

srpm: $(QARSH_TARNAME).tar.bz2 qarsh.spec
	rpmbuild -ts $< $(RPMDEFS)

# Redefine the default compile rule to include target specific parts
%: %.c
	$(LINK.c) $(CFLAGS_$@) $^ $(LIBS_$@) $(LOADLIBES) -o $@
