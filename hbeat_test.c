/*
 * Copyright © 2005-2006  Red Hat, Inc. All rights reserved.
 *
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions of the
 * GNU General Public License v.2.  This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY expressed or implied,
 * including the implied warranties of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. Any Red Hat
 * trademarks that are incorporated in the source code or documentation are not
 * subject to the GNU General Public License and may only be used or replicated
 * with the express permission of Red Hat, Inc.
 *
 * Red Hat Author(s): Nathan Straz <nstraz@redhat.com>
 *                    Dean Jansa <djansa@redhat.com>
 */
#include <stdio.h>
#include "hbeat.h"

char *state2str(hbeat_state_t state);

int
main(void)
{
	hbeat_t hb1, hb2, hb3;
	int i;
	int timeout_max = 5;

	hb1 = hbeat_init("link-13", timeout_max);
	hb2 = hbeat_init("link-14", timeout_max);
	hb3 = hbeat_init("link-15", timeout_max);

	for (i = 0; i < 200; i++) {
		printf("hbeat(1) = %d\n", hbeat(hb1));
		printf("hbeat state = %s\n", state2str(hbeat_getstate(hb1)));
		printf("hbeat(2) = %d\n", hbeat(hb2));
		printf("hbeat state = %s\n", state2str(hbeat_getstate(hb2)));
		printf("hbeat(3) = %d\n", hbeat(hb3));
		printf("hbeat state = %s\n", state2str(hbeat_getstate(hb3)));
		sleep(1);
	}

	hbeat_free(hb1);
	hbeat_free(hb2);
	hbeat_free(hb3);
	
	return 0;
}

char *
state2str(hbeat_state_t state)
{
	char *cp;

	switch (state) {
		case HOST_ALIVE:
			cp = "HOST_ALIVE";
			break;
		case HOST_QUIET:
			cp = "HOST_QUIET";
			break;
		case HOST_TIMEOUT:
			cp = "HOST_TIMEOUT";
			break;
		case HOST_REBOOT:
			cp = "HOST_REBOOT";
			break;
		case HOST_HBEAT_DISABLED:
			cp = "HOST_HBEAT_DISABLED";
			break;
	}

	return cp;

}
		
