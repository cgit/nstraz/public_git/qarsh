Summary:	QA Remote Shell
Name:		qarsh
Version:	2.4
Release:	1%{?dist}
Group:		Applications/Internet
License:	GPL
Buildroot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRequires: systemd

Source0: qarsh-%{version}.tar.bz2

%description
qarsh is an rsh/ssh replacement designed for a testing environment.  That means
no encryption and no authentication.  It also means that the return code is the
same as the remote process and some signals are propogated to the remote
process.  The qarsh package provides the client side programs for
copying files and starting remote commands.  Qarsh is not fit for
interactive commands.

%package server
Group:		System Environment/Daemons
Summary:	QA Remote Shell Server
Requires: systemd
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description server
qarsh is an rsh/ssh replacement designed for a testing environment.  That means
no encryption and no authentication.  It also means that the return code is the
same as the remote process and some signals are propogated to the remote
process.  The qarsh-server package provides the server for qarsh, which
allows anyone to connect to the host as any user and do any thing.

WARNING: THIS PACKAGE PROVIDES REMOTE ROOT ACCESS WITHOUT AUTHENTICATION 


%package selinux
Summary:	SELinux policy module supporting qarsh
Group:		System Environment/Daemons
Requires:	%{name}-server = %{version}-%{release}
Requires(post):	/usr/sbin/semodule, /sbin/fixfiles, gxpp, make
Requires(post):	checkpolicy, /usr/share/selinux/devel/policy.xml, hardlink
Requires(post):	/usr/share/selinux/devel/Makefile
Requires(postun):	/usr/sbin/semodule


%description selinux
SELinux policy maker for qarsh

%prep

%setup -q

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install INSTROOT=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%post server
%systemd_post btimed.service
%systemd_post btimed.socket
%systemd_post qarshd@.service
%systemd_post qarshd.socket

%preun server
%systemd_preun btimed.service
%systemd_preun btimed.socket
%systemd_preun qarshd@.service
%systemd_preun qarshd.socket

%postun server
%systemd_postun_with_restart btimed.service
%systemd_postun_with_restart btimed.socket
%systemd_postun_with_restart qarshd@.service
%systemd_postun_with_restart qarshd.socket

%post selinux
cd %{_docdir}/qarsh-selinux-%{version} && sh rebuild-policy.sh
/sbin/fixfiles -R qarsh-server restore || :

%postun selinux
if [ $1 = 0 ]; then
	/usr/sbin/semodule -r qarshd || :
fi

%files
%defattr(-,root,root)
/usr/bin/qarsh
/usr/bin/qacp
/usr/bin/btimec
%doc %{_mandir}/man1/*

%files server
%defattr(-,root,root)
/usr/sbin/qarshd
/usr/sbin/btimed
%{_unitdir}/*
%doc %{_mandir}/man8/*

%files selinux
%defattr(-,root,root)
%doc SELinux/*


%changelog
* Tue Sep 16 2014 Nate Straz <nstraz@redhat.com> 2.4-1
- [qarsh] Add recv_packet with selects and hbeat
- [qarsh] Mark the node alive after we get a packet
- [qarsh] Remove extra ':' from error message

* Wed Aug 20 2014 Nate Straz <nstraz@redhat.com> 2.3-1
- [sockutil] Handle EINTR in send/recv_packet

* Wed Aug 20 2014 Nate Straz <nstraz@redhat.com> 2.2-1
- [sockutil] Rewrite send_packet() so it retries

* Tue Jul 22 2014 Nate Straz <nstraz@redhat.com> 2.1-1
- [qarshd] Fix timeout when child exits
- Include host name in reboot and host down messages
- Add -T option as a no-op
- Don't check childfds after we've closed one.

* Mon Dec 09 2013 Nate Straz <nstraz@redhat.com> 2.0-1
- Bump version to 2.0 for new release

* Wed Oct 02 2013 Nate Straz <nstraz@redhat.com>
+ qarsh-1.92-1
- Changes to qacp protocol and error handling

* Mon Sep 23 2013 Nate Straz <nstraz@redhat.com>
+ qarsh-1.90-1
- Test build with new protocol

* Thu Jul 19 2012 Nate Straz <nstraz@redhat.com>
+ qarsh-1.28-1
- Restore original sigmask before exec'ing child 

* Wed Jun 27 2012 Nate Straz <nstraz@redhat.com>
+ qarsh-1.27-1
- support IPv6 connections
- Don't limit instances of qarshd in xinetd
- Fix exit code for connection failures.
- Check return of send_package and exit on error
- Pass hints into getaddrinfo

* Fri Aug 20 2010 Nate Straz <nstraz@redhat.com>
+ qarsh-1.26-1
- Build selinux policy at install time
- Clean up parent's open file descriptors

* Fri Nov 20 2009 Nate Straz <nstraz@redhat.com>
+ qarsh-1.25-1
- Handle growing files better in qacp
- Add quiet option to btimec
- Only look up local user if remote is not specified
- Add SE Linux policy from Jaroslav Kortus

* Thu Oct 22 2009 Nate Straz <nstraz@redhat.com>
+ qarsh-1.24-3
- Add SELinux policy build

* Fri Apr 17 2009 Nate Straz <nstraz@redhat.com> 
+ qarsh-1.24-2
- Fix up spec file for tarballs with prefixes

* Fri Apr 17 2009 Nate Straz <nstraz@redhat.com>
+ qarsh-1.24-1
- Handle very broken servers where libxml2.so went missing
- Return proper exit codes from btimec
- Wait longer for a btime response
- Force btimed to be an IPv4 service
- Fix qarsh so it doesn't get stuck after a command exits

* Mon Nov 10 2008 Nate Straz <nstraz@redhat.com>
+ qarsh-1.23-1
- Speed up qarsh by not waiting for an extra pselect

* Fri Oct 31 2008 Nate Straz <nstraz@redhat.com>
+ qarsh-1.22-1
- Fix double-free and command hang in qarsh

* Tue Oct 14 2008 Nate Straz <nstraz@redhat.com>
+ qarsh-1.21-1
- Fix double-free in qacp file to host:file
- Calculate boot time from /proc/uptime

* Fri Oct 10 2008 Nate Straz <nstraz@redhat.com>
+ qarsh-1.20-1
- Fix the memory leak fix.

* Fri Oct 10 2008 Nate Straz <nstraz@redhat.com>
+ qarsh-1.19-1
- Fix memory leaks in qacp
- Fix a CPU hogging spin after a command exits

* Tue Jul 08 2008  1.18-2
- Include new man pages. 

* Tue Jul 08 2008  1.18-1
- Make sure we process all output after the command exits.

* Thu Jul 03 2008  1.17-1
- Use select() to control writes in qarsh.
- chdir() to home directory if possible.
- Add debug option to syslog debug messages.

* Mon Feb 11 2008  1.16-1
- Add copyright headers
- Use select in btime() so higher latency connections work.

* Tue Feb 20 2007  1.15-1
- Make sure qarsh exits with the same signal as the remote side.

* Wed Feb 14 2007 1.14-1
- Remove the alarm in btimed so it will stick around.

* Mon Feb 12 2007  1.13-1
- Remove the per_source limits.

* Thu Nov 16 2006  1.12-1 
- Fix working with large files and un-sendfile-able files.

* Fri Aug 11 2006  1.11-1 
- Change the retry settings to make heartbeating more reliable.

* Thu Aug 03 2006 Nathan Straz <nstraz@redhat.com> 1.10-1
- Add cookies to the heartbeat packet.

* Thu Jun 08 2006  1.9-2 
- Add some debugging messages to find the mysterious client rebooted bug.

* Fri Mar 17 2006 Nathan Straz <nstraz@redhat.com> 1.9-1 
- Add a -r (readable) flag to btimec
- Enhance qarsh command line parsing to make rsync happy again.

* Wed Feb 08 2006 Nathan Straz <nstraz@redhat.com> 1.8-1 
- Fix fake reboot bug.

* Mon Jan 16 2006 Nathan Straz <nstraz@redhat.com> 1.7-1
- Make error messages more useful when there are connection issues. 

* Thu Jan 05 2006 Nathan Straz <nstraz@redhat.com> 1.6-1
- Make btimed stick around

* Thu Oct 6 2005 Dean Jansa <djansa@redhat.com> 1.2-2 
- Use hbeat lib interface for heartbeat

* Mon Sep 26 2005  Nathan Straz <nstraz@redhat.com> 1.2-1 
- New version that fixes a bug in heartbeat.

* Fri Sep 23 2005 Nathan Straz <nstraz@redhat.com> 1.1-1 
- New version with timeout capability

* Tue Sep 13 2005 Nathan Straz <nstraz@redhat.com> 1.0-1 
- Add xinetd files
- Split package into client and server parts

* Thu Sep 13 2005 Nathan Straz <nstraz@redhat.com>
- Initial packaging
