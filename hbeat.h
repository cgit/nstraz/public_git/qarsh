/*
 * hbeat.h
 *
 * Copyright © 2005,2006  Red Hat, Inc. All rights reserved.
 *
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions of the
 * GNU General Public License v.2.  This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY expressed or implied,
 * including the implied warranties of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. Any Red Hat
 * trademarks that are incorporated in the source code or documentation are not
 * subject to the GNU General Public License and may only be used or replicated
 * with the express permission of Red Hat, Inc.
 *
 * Red Hat Author(s): Nathan Straz <nstraz@redhat.com>
 *                    Dean Jansa <djansa@redhat.com>
 */

typedef void * hbeat_t;

typedef enum {
	HOST_ALIVE, HOST_QUIET, HOST_TIMEOUT, HOST_REBOOT, HOST_HBEAT_DISABLED
} hbeat_state_t;


extern hbeat_t hbeat_init(const char *host, int max_timeout);
extern void hbeat_free(hbeat_t hbh);
extern unsigned int hbeat(hbeat_t hbh);
extern hbeat_state_t hbeat_getstate(hbeat_t hbh);
extern void hbeat_setstate(hbeat_t hbh, hbeat_state_t state);
extern int hbeat_getmaxtimeout(hbeat_t hbh);
