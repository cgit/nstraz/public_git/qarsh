/*
 * Copyright © 2005  Red Hat, Inc. All rights reserved.
 *
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions of the
 * GNU General Public License v.2.  This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY expressed or implied,
 * including the implied warranties of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. Any Red Hat
 * trademarks that are incorporated in the source code or documentation are not
 * subject to the GNU General Public License and may only be used or replicated
 * with the express permission of Red Hat, Inc.
 *
 * Red Hat Author(s): Nathan Straz <nstraz@redhat.com>
 *                    Dean Jansa <djansa@redhat.com>
 */
#ifndef _QARSH_PACKET_H
# define _QARSH_PACKET_H	1

#include <sys/types.h>
#include <sys/stat.h>

#define QARSH_MAX_PACKET_SIZE 128*1024

enum qa_packet_type {
	QP_INVALID = 0,
	QP_HELLO = 1,
	QP_RETURNCODE = 2,
	QP_RUNCMD = 3,
	QP_ACK = 4,
	QP_CMDEXIT = 5,
	QP_SETUSER = 6,
	QP_KILL = 7,
	QP_RECVFILE = 8,
	QP_SENDFILE = 9,
	QP_RSTAT = 10,
	QP_DATA = 11,
	QP_DALLOW = 12
};

struct qp_hello_pkt {
	char *qp_greeting;
};

struct qp_returncode_pkt {
	int qp_rc;
	int qp_errno;
	char *qp_strerror;
};

struct qp_runcmd_pkt {
	char *qp_cmdline;
};

/* General packet for acknowledging a command worked */
struct qp_ack_pkt {
	enum qa_packet_type qp_ack_type;
	int qp_ack_seq;
};

struct qp_cmdexit_pkt {
	pid_t qp_pid;
	int qp_status;
};

struct qp_setuser_pkt {
	char *qp_user;
	char *qp_group;
};

struct qp_kill_pkt {
	int qp_sig;
};

struct qp_recvfile_pkt {
	char *qp_path;
	mode_t qp_mode;
	off_t qp_count;
};

struct qp_sendfile_pkt {
	char *qp_path;
};

struct qp_rstat_pkt {
	char *qp_path;
	mode_t qp_st_mode;
	uid_t qp_st_uid;
	gid_t qp_st_gid;
	off_t qp_st_size;
};

struct qp_data_pkt {
	int qp_remfd;		/* remote fd to write to */
	int qp_count;		/* length of data buffer */
	int free_blob;		/* Whether qpfree should free the blob */
	void *qp_blob;
};

struct qp_data_allow_pkt {
	int qp_remfd;		/* fd we're allowing data on */
	int qp_count;		/* How much data the receiver is allowed to send */
};

struct qa_packet {
	enum qa_packet_type qp_type;
	int qp_seq;	/* Sequence number for this packet */
	union {
		struct qp_hello_pkt hello;
		struct qp_returncode_pkt returncode;
		struct qp_runcmd_pkt runcmd;
		struct qp_ack_pkt ack;
		struct qp_cmdexit_pkt cmdexit;
		struct qp_setuser_pkt setuser;
		struct qp_kill_pkt kill;
		struct qp_recvfile_pkt recvfile;
		struct qp_sendfile_pkt sendfile;
		struct qp_rstat_pkt rstat;
		struct qp_data_pkt data;
		struct qp_data_allow_pkt dallow;
	} qp_u;
};

#define qp_hello qp_u.hello
#define qp_returncode qp_u.returncode
#define qp_runcmd qp_u.runcmd
#define qp_ack qp_u.ack
#define qp_cmdexit qp_u.cmdexit
#define qp_setuser qp_u.setuser
#define qp_kill qp_u.kill
#define qp_recvfile qp_u.recvfile
#define qp_sendfile qp_u.sendfile
#define qp_rstat qp_u.rstat
#define qp_data qp_u.data
#define qp_dallow qp_u.dallow

/* Prototypes */
char *qp_packet_type(enum qa_packet_type t);
struct qa_packet *parse_packet(char *buf, int buflen);
struct qa_packet *make_qp_hello(char *greeting);
struct qa_packet *make_qp_returncode(int rc, int eno, char *strerr);
struct qa_packet *make_qp_ack(enum qa_packet_type t, int i);
struct qa_packet *make_qp_runcmd(char *cmdline);
struct qa_packet *make_qp_cmdexit(pid_t pid, int status);
struct qa_packet *make_qp_setuser(char *user, char *group);
struct qa_packet *make_qp_kill(int sig);
struct qa_packet *make_qp_recvfile(const char *path, off_t count, mode_t mode);
struct qa_packet *make_qp_sendfile(const char *path);
struct qa_packet *make_qp_rstat(const char *path, const struct stat *sb);
struct qa_packet *make_qp_data(const int remfd, const int count, void *blob);
struct qa_packet *make_qp_data_allow(const int remfd, const int count);
int qptostr(struct qa_packet *qp, char *qpstr, int maxsize);
void qpfree(struct qa_packet *qp);
void dump_qp(struct qa_packet *qp);

#endif /* !_QARSH_PACKET_H */
