/* 
 * btimec.c --
 *
 *	Example program for libbtime
 * 
 * Copyright © 2005,2006  Red Hat, Inc. All rights reserved.
 *
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions of the
 * GNU General Public License v.2.  This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY expressed or implied,
 * including the implied warranties of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. Any Red Hat
 * trademarks that are incorporated in the source code or documentation are not
 * subject to the GNU General Public License and may only be used or replicated
 * with the express permission of Red Hat, Inc.
 *
 * Red Hat Author(s): Nathan Straz <nstraz@redhat.com>
 *                    Dean Jansa <djansa@redhat.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "btime.h"

char usage[] = "USAGE: %s [-qr] <host>\n";

int
main(int argc, char **argv)
{
	unsigned int bt;
	char *btimedhost;
	int readable = 1;
	int c;
	int ret = 0;

	while ((c = getopt(argc, argv, "qr")) != -1) {
		switch (c) {
			case 'q':
				readable = 0;
				break;
			case 'r': 
				readable = 2; 
				break;
			default: 
				fprintf(stderr, usage, argv[0]);
				exit(2);
		}
	}

	if (optind >= argc) {
		fprintf(stderr, usage, argv[0]);
		exit(2);
	}
	srandom(time(0));
	for ( ; optind < argc; optind++) {
		btimedhost = argv[optind];
		bt = btime(btimedhost);
		if (readable == 2) {
			time_t tt = bt;
			time_t *ttp = &tt;
			printf("%s: %s", btimedhost, bt ? ctime(ttp) : "down\n");
		} else if (readable == 1) {
			printf("%s: %u\n", btimedhost, bt);
		}
		if (bt == 0) ret = 1;
	}
	exit(ret);
}
