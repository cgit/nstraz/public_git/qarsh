#!/bin/bash
#
# Try really hard to rebuild SELinux policy
#
tmpdir=$(mktemp -d -t qarshd-rebuild-policy.XXXX)
savetmp=""

mark_bad()
{
	echo "Avoiding: $1"
	echo $1 >> qarshd.bad-interfaces
}

get_rule()
{
        line=`head -n $1 qarshd.te | tail -n 1`
        line=${line%(qarshd_t)\')}
        line=${line#optional_policy(\`}
        echo $line

}

lookup_rule()
{
	lineno=`grep -B1 $1 tmp/qarshd.tmp | awk '{print $NF; exit}'`;
	get_rule $lineno
}

cp -a * $tmpdir
pushd $tmpdir

for cmd in "make" "make load"; do 
	until output=$($cmd 2>&1); do
		badlineno=`echo "$output" | perl -n -e'/^qarshd.te":(\d+):ERROR/ && print "$1"'`
		if [ -n "$badlineno" ]; then
			badint=`get_rule $badlineno`
			if [ -n "$badint" ]; then
				mark_bad $badint
				echo "$output" > $tmpdir/$badint.log
				savetmp="yes"
				continue
			fi
		fi
		badtype=`echo "$output" | perl -n -e'/libsepol.print_missing_requirements.*attribute (\S+)/ && print "$1\n"'`
		if [ -n "$badtype" ]; then
			badint=`lookup_rule $badtype`
			if [ -n "$badint" ]; then
				mark_bad $badint
				echo "$output" > $tmpdir/$badint.log
				savetmp="yes"
				continue
			else
				echo "Interface not found from bad type $badtype"
				exit 1
			fi
		fi
		conflict=`echo "$output" | awk '/conflicting TE rule for \(qarshd_t/ { print $NF; exit }'`
		if [ -n "$conflict" ]; then
			badint=`lookup_rule $conflict`
			if [ -n "$badint" ]; then
				mark_bad $badint
				echo "$output" > $tmpdir/$badint.log
				savetmp="yes"
				continue
			else
				echo "Interface not found from conflict type $conflict"
				exit 1
			fi
		fi
		dupe=`echo "$output" | awk '/duplicate TE rule for qarshd_t/ { print $NF; exit }'`
		if [ -n "$dupe" ]; then
			badint=`lookup_rule $dupe`
			if [ -n "$badint" ]; then
				mark_bad $badint
				echo "$output" > $tmpdir/$badint.log
				savetmp="yes"
				continue
			else
				echo "Interface not found from duplicate type $dupe"
				exit 1
			fi
		fi
		echo "Error from $cmd not understood"
		echo "------"
		echo "$output"
		echo "------"
		echo "$output" > $tmpdir/$cmd.log
		exit 1
	done
done
popd

if [ -z "$savetmp" ]; then
	rm -rf $tmpdir
else
	echo "Saved policy rebuild logs in $tmpdir"
fi
