/*
 * btime_int.h --
 *
 * Copyright © 2005,2006  Red Hat, Inc. All rights reserved.
 *
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions of the
 * GNU General Public License v.2.  This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY expressed or implied,
 * including the implied warranties of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. Any Red Hat
 * trademarks that are incorporated in the source code or documentation are not
 * subject to the GNU General Public License and may only be used or replicated
 * with the express permission of Red Hat, Inc.
 *
 * Red Hat Author(s): Nathan Straz <nstraz@redhat.com>
 *                    Dean Jansa <djansa@redhat.com>
 */

#ifndef __BTIME_INT_H
#define __BTIME_INT_H

#define BTIME_PORT   5016
#define BTIME_MSGLEN 128
#define MAX_RETRY    40

#define COOKIE_RANDOM_PARTS 4
#define COOKIE_LEN (3 + (COOKIE_RANDOM_PARTS * sizeof(int32_t)))
#endif /* __BTIME_INT_H */
